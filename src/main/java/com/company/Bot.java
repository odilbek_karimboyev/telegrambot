package com.company;


import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;


import java.util.ArrayList;
import java.util.List;

public class Bot extends TelegramLongPollingBot {
    public static void runBot() {
        try {
            TelegramBotsApi api = new TelegramBotsApi(DefaultBotSession.class);
            api.registerBot(new Bot());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return "Karim0v";
    }

    @Override
    public String getBotToken() {
        return "1821535360:AAEAIowfu8P0eR_8IYQP409wzQSmi8cf78s";
    }

    @Override
    public void onUpdateReceived(Update update) {
        String usdRate = BankService.bankApi().get("USD").getRate();
        String eurRate = BankService.bankApi().get("EUR").getRate();
        String jpyRate = BankService.bankApi().get("JPY").getRate();

        SendMessage send = new SendMessage();
        send.setChatId(String.valueOf(update.getMessage().getChatId()));

        // keybords
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> keyboard = new ArrayList<>();

        KeyboardRow firstButton = new KeyboardRow();
        KeyboardRow secondButton = new KeyboardRow();
        KeyboardRow thirdButton = new KeyboardRow();


        // Edit path
        PathController.editPath(update);

        if (update.getMessage().getText().equals("Orqaga")) {
            String nowPath = Path.paths.get(update.getMessage().getChatId());
            Path.paths.replace(update.getMessage().getChatId(), nowPath.substring(0, nowPath.lastIndexOf('/')));
        }


        // controller
        String path = Path.paths.get(update.getMessage().getChatId());

        switch (path.substring(path.lastIndexOf('/') + 1)) {
            case "/start":
            case "start": {
                send.setText("Valyuta ayirboshlash botiga Xush kelibsiz.");
                firstButton.add("Valyuta olish");
                secondButton.add("Valyuta sotish");
                keyboard.add(firstButton);
                keyboard.add(secondButton);
                break;
            }
            case "buy":
            case "sell": {
                send.setText("Valyutani tanlang");
                firstButton.add(0, "Dollar");
                secondButton.add(0, "Euro");
                thirdButton.add("Yuan");
                keyboard.add(0, firstButton);
                keyboard.add(1, secondButton);
                keyboard.add(2, thirdButton);
                break;
            }
            case "dollar": {
                if(path.contains("buy")){
                    send.setText("Pul miqdorini kiriting(So'mda)");
                }
                if(path.contains("sell")){
                    send.setText("Pul miqdorini kiriting(Dollarda)");
                }
                if (path.contains("buy") && !update.getMessage().getText().equals("Dollar")) {
                    double sum = Double.parseDouble(update.getMessage().getText()) / Double.parseDouble(usdRate);
                    String sumString = String.format("%.2f",sum);
                    send.setText(sumString.concat(" $"));
                    firstButton.add("Bosh sahifa");
                    keyboard.add(firstButton);
                } else if (path.contains("sell") && !update.getMessage().getText().equals("Dollar")) {
                    double sum = Double.parseDouble(update.getMessage().getText()) * Double.parseDouble(usdRate);
                    String sumString = String.format("%.2f",sum);
                    send.setText(sumString.concat(" so'm"));
                    firstButton.add("Bosh sahifa");
                    keyboard.add(firstButton);
                    break;
                }
                break;
            }
            case "euro": {
                if(path.contains("buy")){
                    send.setText("Pul miqdorini kiriting(So'mda)");
                }
                if(path.contains("sell")){
                    send.setText("Pul miqdorini kiriting(Euroda)");
                }
                if (path.contains("buy") && !update.getMessage().getText().equals("Euro")) {
                    double sum = Double.parseDouble(update.getMessage().getText()) / Double.parseDouble(eurRate);
                    String sumString = String.format("%.2f",sum);
                    send.setText(sumString.concat(" Euro"));
                    firstButton.add("Bosh sahifa");
                    keyboard.add(firstButton);
                    break;
                } else if (path.contains("sell") && !update.getMessage().getText().equals("Euro")) {
                    double sum = Double.parseDouble(update.getMessage().getText()) * Double.parseDouble(eurRate);
                    String sumString = String.format("%.2f",sum);
                    send.setText(sumString.concat(" so'm"));
                    firstButton.add("Bosh sahifa");
                    keyboard.add(firstButton);
                    break;
                }
                break;
            }
            case "yuan": {
                if(path.contains("buy")){
                    send.setText("Pul miqdorini kiriting(So'mda)");
                }
                if(path.contains("sell")){
                    send.setText("Pul miqdorini kiriting(Yuanda)");
                }
                if (path.contains("buy") && !update.getMessage().getText().equals("Yuan")) {
                    double sum = Double.parseDouble(update.getMessage().getText()) / Double.parseDouble(jpyRate);
                    String sumString = String.format("%.2f",sum);
                    send.setText(sumString.concat(" Yuan"));
                    firstButton.add("Bosh sahifa");
                    keyboard.add(firstButton);
                    break;
                } else if (path.contains("sell") && !update.getMessage().getText().equals("Yuan")) {
                    double sum = Double.parseDouble(update.getMessage().getText()) * Double.parseDouble(jpyRate);
                    String sumString = String.format("%.2f",sum);
                    send.setText(sumString.concat(" so'm"));
                    firstButton.add("Bosh sahifa");
                    keyboard.add(firstButton);
                    break;
                }
                break;
            }
        }

        if (!Path.paths.get(update.getMessage().getChatId()).equals("/start")) {
            KeyboardRow backButton = new KeyboardRow();
            backButton.add("Orqaga");
            keyboard.add(backButton);
        }

        replyKeyboardMarkup.setKeyboard(keyboard);
        send.setReplyMarkup(replyKeyboardMarkup);
        try {
            execute(send);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

}
