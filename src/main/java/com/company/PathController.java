package com.company;

import org.telegram.telegrambots.meta.api.objects.Update;

public class PathController {
    public static void editPath(Update update) {
        if (update.getMessage().getText().equals("/start")) {
            if (!Path.paths.containsKey(update.getMessage().getChatId())) {
                Path.paths.put(update.getMessage().getChatId(), "/start");
            } else {
                Path.paths.replace(update.getMessage().getChatId(), "/start");
            }
        }
        if (update.getMessage().getText().equals("Bosh sahifa")) {
            Path.paths.replace(update.getMessage().getChatId(), "/start");
        }
        if (update.getMessage().getText().equals("Valyuta olish")) {
            Path.paths.replace(update.getMessage().getChatId(), "/start/buy");
        }

        if (update.getMessage().getText().equals("Valyuta sotish")) {
            Path.paths.replace(update.getMessage().getChatId(), "/start/sell");
        }

        if (update.getMessage().getText().equals("Dollar")) {
            if (Path.paths.get(update.getMessage().getChatId()).contains("buy")) {
                Path.paths.replace(update.getMessage().getChatId(), "/start/buy/dollar");
            } else if (Path.paths.get(update.getMessage().getChatId()).contains("sell")) {
                Path.paths.replace(update.getMessage().getChatId(), "/start/sell/dollar");
            }
        }

        if (update.getMessage().getText().equals("Euro")) {
            if (Path.paths.get(update.getMessage().getChatId()).contains("buy")) {
                Path.paths.replace(update.getMessage().getChatId(), "/start/buy/euro");
            } else if (Path.paths.get(update.getMessage().getChatId()).contains("sell")) {
                Path.paths.replace(update.getMessage().getChatId(), "/start/sell/euro");
            }
        }

        if (update.getMessage().getText().equals("Yuan")) {
            if (Path.paths.get(update.getMessage().getChatId()).contains("buy")) {
                Path.paths.replace(update.getMessage().getChatId(), "/start/buy/yuan");
            } else if (Path.paths.get(update.getMessage().getChatId()).contains("sell")) {
                Path.paths.replace(update.getMessage().getChatId(), "/start/sell/yuan");
            }
        }
    }
}
