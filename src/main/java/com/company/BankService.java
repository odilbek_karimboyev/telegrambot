package com.company;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BankService {
    public static Map<String, Currency> bankApi() {
        Map<String, Currency> map = new HashMap<>();
        try {
            URL url = new URL("https://cbu.uz/oz/arkhiv-kursov-valyut/json/");
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            URLConnection connection = url.openConnection();
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                List<Currency> currencyList = new ArrayList<>(List.of(gson.fromJson(bufferedReader, Currency[].class)));
                for (Currency currency : currencyList) {
                    if (currency.getCcy().equals("USD")) map.put("USD", currency);
                    if (currency.getCcy().equals("EUR")) map.put("EUR", currency);
                    if (currency.getCcy().equals("JPY")) map.put("JPY", currency);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }
}
